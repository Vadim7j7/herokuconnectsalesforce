Rails.application.routes.draw do

  root 'home#index'

  get '/login' => 'login#index'
  post '/login' => 'login#login'
  get '/logout' => 'login#logout'


  put '/contact' => 'contact#update'

end
