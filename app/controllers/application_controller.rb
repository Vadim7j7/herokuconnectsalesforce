class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :start_app

  private

  def start_app
    @salesforce = Salesforce.new

    if session['user']
      @current_user = @salesforce.user_info(session['user']['id'])
    end
  end


  def is_login
    unless @current_user
      redirect_to login_path
    end
  end

end
