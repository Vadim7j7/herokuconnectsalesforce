class ContactController < ApplicationController
  before_action :is_login

  def update
    @salesforce.update(@current_user['id'], 'Contact', par_contact)

    redirect_to root_path
  end

  private

  def par_contact
    params.require(:contact).permit(:firstname, :lastname, :email, :phone)
  end

end