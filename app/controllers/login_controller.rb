class LoginController < ApplicationController

  def index
  end

  def login
    user = @salesforce.is_email(params[:email])
    if user && params[:password] == Salesforce::PASSWORD
      session['user'] = user
      redirect_to root_path
    else
      render :index
    end
  end

  def logout
    session.delete('user')
    redirect_to '/login'
  end

end