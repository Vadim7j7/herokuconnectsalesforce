class Salesforce

  PASSWORD = '123456789'

  def initialize
    @connection = ActiveRecord::Base.connection
  end

  def is_email(email)
    @connection.execute("select * from salesforce.Contact WHERE email = '#{email}';")[0]
  rescue StandardError
  end

  def user_info(id)
    @connection.execute("select * from salesforce.Contact WHERE id = '#{id}';")[0]
  rescue StandardError
  end

  def update(id, table, data)
    data_str = data.map { |item| "#{item[0]} = '#{item[1]}'" }.join(', ')
    sql = "UPDATE salesforce.#{table} SET #{data_str} WHERE id = #{id}"
    @connection.execute(sql)
  end

end